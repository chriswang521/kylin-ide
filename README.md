# Kylin-IDE
目标是让用户在银河麒麟操作系统上便捷地开发软件

## 链接地址
- IDE基础平台安装包最新版本下载地址：https://gitee.com/chriswang521/kylin-ide/releases
- IDE基础平台历史安装包提供百度网盘下载链接：https://pan.baidu.com/s/1eDm687VFYB-VnytR4chABw?pwd=9999
- [插件手动下载地址](https://gitee.com/openkylin/extensions-repo/tree/master/KylinIDETeam)：https://gitee.com/openkylin/extensions-repo  KylinIDETeam目录
- [用户帮助文档地址](https://gitee.com/openkylin/extensions-repo/blob/master/user-guide/%E7%9B%AE%E5%BD%95.md)：https://gitee.com/openkylin/extensions-repo  user-guide目录


## 支持架构
- 支持X86、ARM,从v1.1.1桌面版本不再支持LoongArch架构，其他架构均通过远程开发插件、WebIDE来支持
- 支持银河麒麟桌面操作系统V10（桌面应用模式、远程开发模式）
- 支持银河麒麟服务器操作系统V10（仅远程开发模式）

## 组成
- **Kylin-IDE**整体上由4部分组成，如下图所示，包括IDE基础平台、插件管理器、插件、插件仓库。**本仓库仅是Kylin-IDE的其中一个组成部分：IDE基础平台**
	* ![总体架构图](./resources/readme-img/structure.png)
- 仅安装IDE基础平台只能完成很有限的编辑功能，需要安装插件并在操作系统上安装插件依赖才能实现更多的开发功能
- IDE基础平台提供基本图形界面、插件架构，用户通过插件管理器检索、安装、管理插件，通过插件依赖管理器安装插件依赖

## 桌面应用模式与远程开发模式
- **桌面应用模式**：与常见IDE一样，IDE基础平台以桌面应用软件形式安装运行
	* [本仓库的kylin-ide](https://gitee.com/chriswang521/kylin-ide/releases)即为这种模式
	* 注意，目前支持桌面操作系统但不支持服务器操作系统（服务器操作系统通常不安装图形界面）
- **远程开发模式**：类似微软remote-ssh功能，IDE基础平台以远端服务形成安装运行，用户在本地通过SSH连接开发远程机器上的代码，体验和本地开发大体一致
	* 远程开发模式需要插件（插件名称：Kylin remote development，插件ID：remote-dev，插件发布者xhafei）和服务端配合实现，插件会自动或引导用户安装服务端
	* 服务端下载地址：https://gitee.com/mcy-kylin/kylin-ide-server/releases
	* 桌面操作系统和服务器操作系统均支持

## 使用方法

### 桌面应用模式
- **下载安装IDE基础平台**
	* [下载安装包](https://gitee.com/chriswang521/kylin-ide/releases)，然后后执行：sudo  dpkg -i <file>.deb
- **下载安装插件依赖管理器**
	* 插件名称：extension dependency，发布者：KylinIdeTeam
	* 安装方式一：通过在线插件检索，从OpenVSX插件市场中安装，见下图：插件视图的搜索输入框中，输入“@id:KylinIdeTeam.extension-dependency”，会在插件列表中搜索到插件依赖管理器。
		- ![插件依赖管理器OpenVSX安装](./resources/readme-img/search.jpg)
	* 安装方式二：或手动在openKylin的[extensions-repo仓库中](https://gitee.com/openkylin/extensions-repo/tree/master/KylinIDETeam/)找到并下载安装插件
- **下载安装插件**
	* 安装方式一：通过在线检索，从OpenVSX插件市场中安装，方法见上文
	* 安装方式二：或手动在openKylin的extensions-repo中找到并下载安装插件
	* Kylin-IDE（KylinCode）插件分类菜单检索到的插件是KylinIdeTeam开发、适配的插件
	* 根据开发需求选择检索分类
- **使用插件依赖管理器检查、安装插件依赖**
	* 点击菜单中的帮助->打开插件依赖管理器进入插件依赖管理器页面
	* 详细见[用户帮助文档相关章节](https://gitee.com/openkylin/extensions-repo/blob/master/user-guide/files/%E6%8F%92%E4%BB%B6%E4%BE%9D%E8%B5%96%E7%AE%A1%E7%90%86.md)
- **开始开发工作**
	* 如果是新手，可以安装Kylin Project Manager插件（项目创建管理插件），创建简单项目，在此基础上进行开发。[详细见用户帮助文档相关章节](https://gitee.com/openkylin/extensions-repo/blob/master/user-guide/files/%E9%A1%B9%E7%9B%AE%E7%AE%A1%E7%90%86.md)
	* 也可以打开一个项目文件夹，开始编辑工作（已安装对应编程语言的支持插件和依赖）；配置task.json、launch.json，运行和调试
		- 以C语言为例，[详细见用户帮助文档相关章节](https://gitee.com/openkylin/extensions-repo/blob/master/user-guide/files/C-C++%E5%BC%80%E5%8F%91.md)

### 远程开发模式
- **在本地桌面系统机器，按照桌面应用模式安装IDE基础平台、插件依赖管理器**
- **下载安装远程开发插件**（插件名称：Kylin remote development，插件ID：remote-dev，插件发布者xhafei）
	* 安装方式一：通过在线插件检索，从OpenVSX插件市场中安装
	* 安装方式二：或手动在openKylin的extensions-repo仓库中找到并下载安装插件
- **输入、配置远程机器的用户名、IP等信息，自动或根据提示安装服务端，输入远程SSH密码**
	* 详细见[用户帮助文档相关章节](https://gitee.com/openkylin/extensions-repo/blob/master/user-guide/files/%E8%BF%9C%E7%A8%8B%E5%BC%80%E5%8F%91.md)
- **在远程端下载安装插件依赖管理器**
- **在远程端下载安装插件**
- **在远程端使用插件依赖管理器检查、安装插件依赖**
- **开始远程开发工作**

## OpenVSX插件市场使用注意
- Kylin-IDE目前默认接入了OpenVSX插件市场。OpenVSX插件市场是一个开源插件市场，所有开发者都可以申请上传插件，插件数量较多。但并不是其中的所有插件都能正常使用，需要用户甄别
- 我们提供了一种检索方式，能够在OpenVSX插件市场中检索到我们开发适配的插件，满足基本开发需求，**请优先使用我们开发适配的插件**
	* 检索方法如下：
		- Kylin-IDE小于V1.1.0版本（Kylin-Code）
			* 在插件页面中，点击侧边栏顶部“筛选”按钮，选择Kylin-IDE（KylinCode）插件分类，可以按照分类检索插件市场中KylinIDETeam开发适配的插件。
			* ![在OpenVSX插件市场中找到我们开发适配的插件](./resources/readme-img/find-our-ext.jpg)
		- Kylin-IDE大于等于V1.1.0
			* 在插件页面中，选择Kylin-IDE（KylinCode）推荐插件视图，点击其视图的标题栏的“筛选器插件...”按钮，可以按照分类检索插件市场中KylinIDETeam开发适配的插件。
			* ![在OpenVSX插件市场中找到我们开发适配的插件](./resources/readme-img/find-our-ext-2.jpg)

## 特色功能
- **[远程开发](#远程开发模式)**
	* 微软的remote-ssh插件为微软协议未开源
	* 支持X86、ARM、LoongArch架构，而remote-ssh插件不支持LoongArch架构。LoongArch架构上的远程开发可以使用本功能
	* 如果有其他架构的支持需要，可以在Issues中提出
	* 详细见[用户帮助文档相关章节](https://gitee.com/openkylin/extensions-repo/blob/master/user-guide/files/%E8%BF%9C%E7%A8%8B%E5%BC%80%E5%8F%91.md)
- **历史调试**
	* 历史调试插件可在调试过程中记录中断时的堆栈信息，并可在调试过程中或结束后回放调试时的堆栈信息，用户可在调试过程中记录关心的调试信息以便在需要的时候复现调试信息
	* 详细见[用户帮助文档相关章节](https://gitee.com/openkylin/extensions-repo/blob/master/user-guide/files/%E5%8E%86%E5%8F%B2%E8%B0%83%E8%AF%95.md)

## 问题反馈
- 请在本仓库或相应仓库的Issues中反馈问题