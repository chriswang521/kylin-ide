# IDE基础平台

Kylin-IDE包括IDE基础平台、插件、插件仓库等内容，目标是让用户在银河麒麟操作系统上便捷地开发软件，目前正在迭代开发中。
IDE基础平台之前的版本基于开源项目Code-OSS（1.68.0）,从1.1.0版本开始IDE基础平台升级Code-OSS为1.85.2版本，默认接入OpenVSX插件商店。我们适配、开发的插件会上传至OpenVSX插件商店，可以通过IDE基础平台定制的插件分类搜索菜单检索到我们的插件；也会上传至openKylin社区的extensions-repo中。

## 下载地址
由于gitee仓库提交单文件大小限制，本仓库提供发行版的最近两个版本，下载地址为：

https://gitee.com/chriswang521/kylin-ide/releases


历史安装包提供百度网盘下载链接为：

https://pan.baidu.com/s/1eDm687VFYB-VnytR4chABw?pwd=9999

## 最新版本
- **1.4.5**

    IDE基础平台迭代至1.4.5版本，支持x64和arm64架构的银河麒麟桌面操作系统V10 SP1。从1.1.0版开始桌面版仅支持x86和arm，其他架构均通过远程开发插件、WebIDE来支持


    | ARCH | 测试适配OS | FILE NAME |
    | -------- | -------- |  -------- |
    | x64    		 | 银河麒麟桌面操作系统 V10SP1 | kylin-ide_1.4.5_amd64.deb |
    | arm64    		 | 银河麒麟桌面操作系统 V10SP1 | kylin-ide_1.4.5_arm64.deb |

    ## 更新日志:

    从v1.2.3到v1.4.5更新如下：

    > 开发环境快速部署：
    - 添加“仅下载不安装（下载软件包、插件形成安装包）”模块功能。
    - 添加“本地安装（离线环境下安装‘仅下载不安装’形成的安装包）”模块功能。
    > IDE基础平台：
    - feat: 帮助菜单下添加汇总仓库(链接)菜单。
    - feat: 关于对话框添加显示引擎code-oss版本，命令行工具中添加参数engine打印引擎code-oss版本。
    - 修复CVE漏洞：CVE-2024-43601，CVE-2024-26165。
    - feat：工具栏添加绘图插件入口图标按钮。
    - 修复一些已知bugs。
    

## 历史版本

- **1.2.3**

    IDE基础平台迭代至1.2.3版本，支持x64和arm64架构的银河麒麟桌面操作系统V10 SP1。从1.1.0版开始桌面版仅支持x86和arm，其他架构均通过远程开发插件、WebIDE来支持


    | ARCH | 测试适配OS | FILE NAME |
    | -------- | -------- |  -------- |
    | x64    		 | 银河麒麟桌面操作系统 V10SP1 | kylin-ide_1.2.3_amd64.deb |
    | arm64    		 | 银河麒麟桌面操作系统 V10SP1 | kylin-ide_1.2.3_arm64.deb |

    ## 更新日志:

    从v1.2.2到v1.2.3更新如下：

    >    - 开发环境快速部署: 开发环境快速部署界面中添加log目录信息 
    >    - 开发环境快速部署: 添加从任务栏关闭窗口的处理逻辑 
    >    - 开发环境快速部署: 细化安装提示信息 
    >    - 修改卸载脚本：非纯净卸载软件包时，保留开发环境快速部署log信息
    >    - fixed:下载及安装插件时突然断网导致界面卡住的问题 

- **1.2.2**
    IDE基础平台迭代至1.2.2版本，支持x64和arm64架构的银河麒麟桌面操作系统V10 SP1.


    | ARCH | 测试适配OS | FILE NAME |
    | -------- | -------- |  -------- |
    | x64    		 | 银河麒麟桌面操作系统 V10SP1 | kylin-ide_1.2.2_amd64.deb |
    | arm64    		 | 银河麒麟桌面操作系统 V10SP1 | kylin-ide_1.2.2_arm64.deb |

    ## 更新日志:

    >    - feat: 重新设计软件icon图标。
    >    - feat: 新增安装deb时添加桌面图标,卸载时删除桌面图标功能。
    >    - feat: 帮助菜单下添加'反馈问题'菜单。
    >    - fixed: 移除远程协议的状态栏图标和欢迎页面的远程按钮。
    >    - fixed: 解决webview的查找功能能够查找到结果.
    >    - build: 修复构建错误。
    >    - feat: 新增开发环境快速部署模块在线安装功能，引导用户根据开发场景在线安装基础功能插件、安装依赖软件、配置基本开发环境。
    >    - 开发环境快速部署: 添加重新加载窗口功能
    >    - 开发环境快速部署: 修复联网校验地址,修改timeout为10000
    >    - 开发环境快速部署: 解决点击取消后,go的js脚本依然执行的
    >    - 开发环境快速部署: 解决点击取消后,sh脚本依然执行的问题
    >    - 开发环境快速部署: fixed: 解决点击跳过闪退
    >    - 开发环境快速部署: fixed:修复因监听到window-all-closed事件导致闪退的问题 
    >    - 开发环境快速部署: fixed: 插件安装取消问题

- **1.1.2**
    IDE基础平台迭代至1.1.2版本，支持x64和arm64架构的银河麒麟桌面操作系统V10 SP1


    | ARCH | 测试适配OS | FILE NAME |
    | -------- | -------- |  -------- |
    | x64    		 | 银河麒麟桌面操作系统 V10SP1 | kylin-ide_1.1.2_amd64.deb |
    | arm64    		 | 银河麒麟桌面操作系统 V10SP1 | kylin-ide_1.1.2_arm64.deb |

    ## 更新日志:

    >    - fixed: 修复IDE基础平台已知bugs。

- **1.1.1**
    IDE基础平台迭代至1.1.1版本（内部测试版本，不对外发布），支持x64和arm64架构的银河麒麟桌面操作系统V10 SP1


    | ARCH | 测试适配OS | FILE NAME |
    | -------- | -------- |  -------- |
    | x64    		 | 银河麒麟桌面操作系统 V10SP1 | kylin-ide_1.1.1_amd64.deb |
    | arm64    		 | 银河麒麟桌面操作系统 V10SP1 | kylin-ide_1.1.1_arm64.deb |

    ## 更新日志:

    >    - fixed: 修复IDE基础平台代码升级到code-oss-1.85.2之后发现的问题;
    >    - feat: 插件侧边栏新增'Kylin-IDE推荐插件'列表视图,列表标题添加筛选和清除筛选按钮,列表单元格显示插件能够安装卸载等
    >    - feat: IDE基础平台的文件菜单下新增“新建项目”菜单和工具栏中新增一个新建项目的按钮,,依赖于项目创建插件KylinIDETeam.project-manager是否安装.

- **1.1.0**
    IDE基础平台迭代至1.1.0版本（内部测试版本，不对外发布），支持x64，arm64架构的银河麒麟桌面操作系统V10 SP1


    | ARCH | 测试适配OS | FILE NAME |
    | -------- | -------- |  -------- |
    | x64    		 | 银河麒麟桌面操作系统 V10SP1 | kylin-ide_1.1.0_amd64.deb |
    | arm64    		 | 银河麒麟桌面操作系统 V10SP1 | kylin-ide_1.1.0_arm64.deb |
    ## 更新日志:

    >    - IDE基础平台代码升级到code-oss-1.85.2;
    >    - IDE基础平台不在支持loongarch64架构.

- **1.0.1**
    IDE基础平台迭代至1.0.1版本，支持x64，arm64和loongarch64架构的银河麒麟桌面操作系统V10 SP1 


    | ARCH | 测试适配OS | FILE NAME |
    | -------- | -------- |  -------- |
    | x64    		 | 银河麒麟桌面操作系统 V10SP1 | kylin-ide_1.0.1_amd64.deb |
    | arm64    		 | 银河麒麟桌面操作系统 V10SP1 | kylin-ide_1.0.1_arm64.deb |
    | loongarch64    | 银河麒麟桌面操作系统 V10SP1 | kylin-ide_1.0.1_loongarch64.deb |

    ## 更新日志:

    >    - 将应用软件安装文件存储目录修改为/opt/apps/kylin-ide;
    >    - desktop文件增加Name[zh_CN]字段和内容;
    >    - png图标补足全尺寸,添加96和512尺寸的图标.

- **1.0.0**
    IDE基础平台迭代至1.0.0版本，支持x64，arm64和loongarch64架构的银河麒麟桌面操作系统V10 SP1 


    | ARCH | 测试适配OS | FILE NAME |
    | -------- | -------- |  -------- |
    | x64    		 | 银河麒麟桌面操作系统 V10SP1 | kylin-ide_1.0.0_amd64.deb |
    | arm64    		 | 银河麒麟桌面操作系统 V10SP1 | kylin-ide_1.0.0_arm64.deb |
    | loongarch64    | 银河麒麟桌面操作系统 V10SP1 | kylin-ide_1.0.0_loongarch64.deb |

    ## 更新日志:

    >    - x64和arm64架构kylin-ide的electron升级为25.8.4版本，以及修复升级中遇到的问题，loogarch64架构kylin-ide的electron维持原理的17.4.7版本。
    >    - 更换软件icon图标。
    >    - 添加Python和jupyter相关插件对api proposal支持。
    >    - 添加更新统计模块。
    >    - 移除打包时版本号中显示的时间。
    >    - 修复部分中文显示不准确的地方。
    >    - 解决反汇编页面中点击调试导航栏的调试按钮时，会退出反汇编视图的问题。

- **0.1.5**
    IDE基础平台迭代至0.1.5版本，支持x64，arm64和loongarch64架构的银河麒麟桌面操作系统V10 SP1 


    | ARCH | 测试适配OS | FILE NAME |
    | -------- | -------- |  -------- |
    | x64    		 | 银河麒麟桌面操作系统 V10SP1 | kylin-ide_0.1.5-1696646012_amd64.deb |
    | arm64    		 | 银河麒麟桌面操作系统 V10SP1 | kylin-ide_0.1.5-1696660452_arm64.deb |
    | loongarch64    | 银河麒麟桌面操作系统 V10SP1 | kylin-ide_0.1.5-1696648591_loongarch64.deb |

    ## 更新日志:

    >    - 修复某些系统（如2303，openkyiln等）右键菜单打开方式中IDE没有图标.	
    >    - 移除webview默认的几个右键菜单.
    >    - 移除任务task菜单中'msbuild,donet,ms编译器问题'等菜单.
    >    - IDE启动耗时太长,新增加载动画..
    >    - 调试状态下,当断点图标显示成白色圆圈线时，增加显示相应悬浮窗口提示.
    >    - 修复git内置插件，push取消输入账号密码时，识别标红终端输出语句为乱码的问题.
    >    - 在线商店中禁止安装eamodio.gitlens,提示使用KylinIdeTeam.gitlens替代.
    >    - 修复调试状态下，变量抽屉视图中通过快捷键F2来设置值光标乱跳.
    >    - 修复 IDE中文显示，对主侧边栏定义不清，不同位置菜单项名称不一致.
    >    - 解决保留旧版本直接安装0.1.4版本,由于有原来的用户数据,首次启动后重载窗体显示英文问题.
    >    - 当product中没有releaseNotesUrl字段时,不注册显示发行说明命令.
    >    - 添加在线插件搜索显示的策略，支持网络地址和product.json配置.

- **0.1.4**
    IDE基础平台迭代至0.1.4版本，支持x64，arm64和loongarch64架构的银河麒麟桌面操作系统V10 SP1（2203） 


    | ARCH        | 测试适配OS                       | FILE NAME                                  |
    | ----------- | -------------------------------- | ------------------------------------------ |
    | x64         | 银河麒麟桌面操作系统 V10SP1-2203 | kylin-ide_0.1.4-1692668996_amd64.deb       |
    | arm64       | 银河麒麟桌面操作系统 V10SP1-2203 | kylin-ide_0.1.4-1692671724_arm64.deb       |
    | loongarch64 | 银河麒麟桌面操作系统 V10SP1-2203 | kylin-ide_0.1.4-1692645082_loongarch64.deb |

    ## 更新日志:

    >    - 修复使用code-server启动时,提示找不到onConfigChange.	
    >    - 修改集成内置简体中文显示插件信息,避免切换商店时,出现重新加载窗体后显示英文.
    >    - 插件卸载时,删除插件安装目录下插件文件.
    >    - 移除debian/postinst.template中设置的apt源，优化代码字段.
    >    - fixed 首次安装重新加载一下窗体，有概率会出现英文界面.解决方法:添加metadata信息,固定中文显示的hash值。
    >    - fixed 终端下命令行安装插件失败，报错：Cannot read properties of undefined (reading 'onConfigChange')


- **0.1.3**

    IDE基础平台迭代至0.1.3版本，支持x64，arm64和loongarch64架构的银河麒麟桌面操作系统V10 SP1（2203） 


    | ARCH | 测试适配OS | FILE NAME |
    | -------- | -------- |  -------- |
    | x64    		 | 银河麒麟桌面操作系统 V10SP1-2203 | kylin-ide_0.1.3-1690359827_amd64.deb |
    | arm64    		 | 银河麒麟桌面操作系统 V10SP1-2203 | kylin-ide_0.1.3-1690360442_arm64.deb |
    | loongarch64    | 银河麒麟桌面操作系统 V10SP1-2203 | kylin-ide_0.1.3-1690361891_loongarch64.deb |

    更新日志:

    >    - Fix项目创建后,帮助菜单缺少插件依赖管理器和离线插件管理器菜单项.
    >    - webview: show find widget with argument.
    >    - 设置中添加插件商店地址配置选项,含有serviceUrl和itemUrl配置.
    >    -  update KylinIDETeam.js-debug version & metadata.
    >    -  设置页面的插件商店配置中添加可信任域TrustDomains.
    >    -  update extensions metadata,delay notification prompt.
    >    -  首次安装IDE，启动后显示开始页面，不自动跳转walkthrough的第一个分类.

- **0.1.2**

    2023年05月19号，IDE基础平台迭代至0.1.2版本，支持x64，arm64和loongarch64架构的银河麒麟桌面操作系统V10 SP1（2203） 
        

    | ARCH | 测试适配OS | FILE NAME | 
    | -------- | -------- |  -------- | 
    | x64    		 | 银河麒麟桌面操作系统 V10SP1-2203 | kylin-ide_0.1.2-1684377176_amd64.deb     |
    | arm64    		 | 银河麒麟桌面操作系统 V10SP1-2203 | kylin-ide_0.1.2-1684380762_arm64.deb     |
    | loongarch64    | 银河麒麟桌面操作系统 V10SP1-2203 | kylin-ide_0.1.2-1684394329_loongarch64.deb  |


    更新日志:

    >    - 卸载不干净，添加如下功能：
    > 
    >        1. deb包,只有当执行dpkg --purge或者-P时,会删除当前用户默>认的Kylin-IDE数据. 
    >
    >        2. rpm包,rpm -e就会删除当前用户默认的Kylin—IDE数据.
    >    -  修改组织名称为KylinIdeTeam
    >    -  修改开启默认插件自动检查更新.
    >    -  帮助菜单和必读中添加跳转到用户帮助文档的菜单和链接。
    >    -  argv.json中添加ignore-certificate-errors参数，启动时获取设置参数,当为true时忽略证书验证.
    >    -  electron webview的查找功能不能使用,临时解决为能够查找到结果，并能查询上/下一个,但是不能全部高亮.

- **0.1.1**

    2023年04月28号，IDE基础平台迭代至0.1.1版本，支持x64，arm64和loongarch64架构的银河麒麟桌面操作系统V10 SP1（2203） 
	
    | ARCH | 测试适配OS | FILE NAME | 
    | -------- | -------- |  -------- | 
    | x64    		 | 银河麒麟桌面操作系统 V10SP1-2203 | kylin-ide_0.1.1-1682578262_amd64.deb     |
    | arm64    		 | 银河麒麟桌面操作系统 V10SP1-2203 | kylin-ide_0.1.1-1682577508_arm64.deb     |
    | loongarch64    | 银河麒麟桌面操作系统 V10SP1-2203 | kylin-ide_0.1.1-1682581086_loongarch64.deb  |

    更新日志:

    >    - Fixed Git帮助链接为vscode链接，应去掉该段文字描述
    >    - Fixed 帮助菜单增加插件依赖管理器和离线插件管理器入口菜单
    >    - Fixed 开始页面“无限扩展性”右侧是显示插件，需要改为我们自己适配和开发的插件. 修改svg中的png图片和搜索命令.
    >    - Fixed 插件搜索-go语言支持-搜索到很多插件;c语言支持搜索果过多. 修改c和go的搜索关键字为:KylinIdeDevCYuYan和KylinIdeDevGolang.
    >    - Fixed js调试插件，插件商店搜索到后，总是提示需要重新加载. 解决法:修改集成js-debug内置插件时的metadata的Id和publisherId 与 open-vsx商店一致.
    >    - Fixed js调试插件，没有进行中文显示. 解决方法:添加插件新I“KylinIDETeam.js-debug”对应的翻译文件.
    >    - Fixed 工具栏中‘源码代码管理’按钮的悬浮提示快捷键错误.
    >    - 文件菜单下,添加选择显示语言的子菜单.
    >    - Fixed 帮助菜单下的'编辑器操场'修改为'编辑器功能'.
    >    - Fixed 工作区信任功能中，应移除外链"了解详细信息"和”我们文档“.
    >    - Fixed 工具栏中‘新建文件’按钮的悬浮提示应该为“新建文本件”.
    >    - Fixed 统一将IDE的extension中文显示从“扩展”更改为“插件”.
    >    - references-view插件中添加metadata数据,其中的uuid来自open-vsx商店生成.

- **0.1.0** 

    2023.04.14号，IDE基础平台迭代的第一个版本0.1.0，支持x64，arm64和loongarch64架构的银河麒麟桌面操作系统V10 SP1（2203） 
	
    | ARCH | 测试适配OS | FILE NAME | 
    | -------- | -------- |  -------- | 
    | x64    		 | 银河麒麟桌面操作系统 V10SP1-2203  | kylin-ide_0.1.0-1681356118_amd64.deb     |
    | arm64    		 | 银河麒麟桌面操作系统 V10SP1-2203  | kylin-ide_0.1.0-1681358356_arm64.deb     |
    | loongarch64    | 银河麒麟桌面操作系统 V10SP1-2203  | kylin-ide_0.1.0-1681366481_loongarch64.deb   |
    
    更新日志:
    
    >    - 第一个基础测试版本。